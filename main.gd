extends Node2D

var screen_size

# Movement
var player_movement_speed = -200
var player_drag = 50
var fuel = 500
var fuel_consumption = 2
var altitude

# Preloads
# Clouds
var cloud = preload("res://cloud_0.tscn")
var cloud_count = 0
var cloud_array = []
var time_since_last_cloud = 0
# TODO powerups

func _ready():
	screen_size = get_viewport_rect().size
	set_process(true)

var game_running = true

func _process(delta):
	if game_running == true:
		main(delta)
	else:
		pass
		
func main(delta):
	var player = get_node("player/player_rigid_body")
	var player_pos = player.get_pos()
	
	var ground_pos = get_node("ground/ground_rigid_body").get_pos()
	
	var player_camera_pos = get_node("player/player_camera")
	
	var player_fuel_label_pos = get_node("player_fuel_label")
	var player_alt_label = get_node("player_alt_label")
	
	get_node("player_fuel_label").set_text("Fuel: " + String(fuel))

	altitude = ground_pos.y - player_pos.y
	get_node("player_alt_label").set_text("Altitude: "+ String(round(altitude)))

	# Input
	if (fuel != 0):
		if (Input.is_action_pressed("ui_up") or get_node("touch_up").is_pressed()):
			#if (fuel != 0):  # Check if we have any fuel left.
			fuel -= fuel_consumption # Deplete fuel as we use it.
			player.set_applied_force(Vector2(0, player_movement_speed))
		else:
			player.set_applied_force(Vector2(0,player_drag)) # Else drag us down.
	
		if (Input.is_action_pressed("ui_left")  or get_node("touch_left").is_pressed()):
			#if (fuel != 0):  # Check if we have any fuel left.
			fuel -= fuel_consumption
			player.set_applied_force(Vector2(player_movement_speed, 0))
	
		if (Input.is_action_pressed("ui_right")  or get_node("touch_right").is_pressed()):
			#if (fuel != 0):  # Check if we have any fuel left.
			fuel -= fuel_consumption
			player.set_applied_force(Vector2(abs(player_movement_speed), 0))
			
		###### Multi-directional ######
		if (Input.is_action_pressed("ui_up") and Input.is_action_pressed("ui_right")):
			fuel -= fuel_consumption
			player.set_applied_force(Vector2(abs(player_movement_speed), player_movement_speed))
			
		if (Input.is_action_pressed("ui_up") and Input.is_action_pressed("ui_left")):
			fuel -= fuel_consumption
			player.set_applied_force(Vector2(player_movement_speed, player_movement_speed))
	else:
		player.set_applied_force(Vector2(0,player_drag)) # Else drag us down.

	# Add more fuel
	if (Input.is_action_pressed("ui_accept")):
		fuel = 1000
	
	# Keep the camera and UI with the player
	player_camera_pos.set_pos(player_pos)
	player_fuel_label_pos.set_pos(Vector2(player_pos.x +20, player_pos.y - 100))
	player_alt_label.set_pos(Vector2(player_pos.x +23 , player_pos.y - 70))
	
	# Generate clouds.
	generate_cloud(delta)

# Creates clouds, adds them to a list and sets where they will spawn.
func new_cloud():
	cloud_count += 1 # Add 1 to cloud counter.
	var cloud_instance = cloud.instance() # Create a cloud.
	cloud_instance.set_name("cloud_"+str(cloud_count)) # Give it a name so we can reference it's position.
	add_child(cloud_instance) # Add it to our list.
	
	var cloud_position = get_node("cloud_"+str(cloud_count)).get_pos()
	var player_camera_pos = get_node("player/player_camera").get_pos()
	var player_camera_view_size = get_node("player/player_camera").get_viewport_rect().size
	
	# Set where the clouds will spawn.
	cloud_position.y = rand_range(player_camera_pos.y + -player_camera_view_size.y, player_camera_pos.y + (-player_camera_view_size.y * 2)) # Generate clouds outside where the player can see.
	cloud_position.x = rand_range(player_camera_pos.x - 150, player_camera_pos.x + 150) 
	
	get_node("cloud_"+str(cloud_count)).set_pos(cloud_position) # Add the cloud to a list so we can reference it later.
	cloud_array.push_back("cloud_"+str(cloud_count))
	print(str(-player_camera_view_size.y ))

# Sets how often clouds will appear.
func generate_cloud(delta):
	time_since_last_cloud += delta
	if time_since_last_cloud > rand_range(0.2, 20): # Set how often clouds spawn.
		new_cloud()
		time_since_last_cloud = 0	
